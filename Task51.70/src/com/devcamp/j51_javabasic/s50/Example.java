package com.devcamp.j51_javabasic.s50;

import java.util.ArrayList;

public class Example {

    /**
     * @param args
     */
    public static void main(String[] args) {
        ArrayList<Integer> myArrList = new ArrayList<>();
            myArrList.add(12);
            myArrList.add(13);
            myArrList.add(14);
            myArrList.add(15);
            myArrList.add(16);
            myArrList.add(17);
        //1. int lastIndexOf(): Lấy vị trí xuất hiện cuối cùng của phần tử
       // System.out.println(myArrList.lastIndexOf(14)); 
        //2, boolean add(int index, Object o): Thêm một phần tử vào vị trí index.
        myArrList.add(5, 90);
       // System.out.println(myArrList);
        //3, boolean remove(Object o): Xoá object o khỏi ArrayList, object o này phải chứa trong ArrayList.
        myArrList.remove((Integer) 17);
       // System.out.println(myArrList);
        //4, boolean remove(int index): Xoá một phần tử tại vị trí index
        myArrList.remove(5);
       // System.out.println(myArrList);
        //5, Object set(int index, Object o): Cập nhật phần tử tại vị trí index.
        myArrList.set(4,100);
       // System.out.println(myArrList);
        //6, int indexOf(Object o): Lấy vị trí index của object o trong ArrayList.
       // System.out.println(myArrList);
       // System.out.println(myArrList.indexOf(100));
        //7, Object get(int index): Return object tại vị trí index trong ArrayList.
       // System.out.println(myArrList.get(3));
        //8, int size(): lấy số lượng phần tử chứa trong ArrayList
       // System.out.println("số phần tử của arraylist " + myArrList.size());
        //9, boolean contains(Object o): Kiểm tra phần tử object o có chứa trong ArrayList, nếu có return true, ngược lại false
        System.out.println(myArrList.contains(100));
        System.out.println(myArrList.contains(200));
        //10, void clear(): Xoá tất cả các phần tử trong ArrayList
        myArrList.clear();
        System.out.println(myArrList);
    }

}
